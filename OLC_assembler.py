##### Acquisition des données #####
# 'wget -r -np -nH --cut-dirs=3 -R index.html http://bioinformatique.rennes.inria.fr/ALG2019/data/' & rm $(find . -name "*html*")

import os, sys, getopt, re, time
import linecache
from collections import defaultdict
sys.setrecursionlimit(100000)


# Définition de la règle "reverse complement"
REVERSE_DICT = {'A':'T', 'T':'A', 'C':'G', 'G':'C'}

def load_from_directory(path,index, is_fasta):
    '''
    Renvoie la ligne d'un fichier correspondant à l'index choisi.

    Arguments:
        path {String} -- Chemin d'accès du fichier
        index {int} -- Numéro d'index de la ligne à charger
        is_fasta {bool} -- Déclarer True si le fichier est au format fasta
    
    Returns:
        String -- Ligne 'index' du fichier stocké au chemin 'path'
    '''
    if is_fasta:
        return linecache.getline(path, index*2+2)[:-1]
    else:
        return linecache.getline(path, index*4+2)[:-1]

def load_reads(start = 0, stop = 100, black_list = list()):
    '''
    Génération d'une liste de reads de taille stop-start, avec une omission des reads présents 
    dans la black_list. Un read peu résolutif (présence de N) ne sera pas chargé.

    Arguments:
        start {int} -- Numéro d'index du début de chargement des reads
        stop {int} -- Numéro d'index de fin de chargement des reads
        black_list {list} -- Liste des index de read à bannir de l'alignement, car déjà positionnés en amont 
    
    Returns:
        dict -- Dictionnaire associant chaque index à la séquence nucléotidique associée
    '''
    reads = dict()
    ind = start
    while ind in black_list:
        ind += 1
        stop += 1
    while ind < stop and ind < nb_reads: # Mise au point de la liste des reads à charger
            tmp = load_from_directory(reads_path, ind, format_fasta)
            if 'N' not in tmp: # filtre read contenant au moins 1 'N'
                reads[ind] = tmp
            else :
                stop += 1
            ind += 1 
            while ind in black_list:
                ind += 1
                stop += 1
    return reads

def reverse_seq(read):
    """ 
    Renvoie l'inverse complément d'une séquence nucléotidique
    
    Arguments:
        read {String} -- Séquence nucléotidique 
    
    Returns:
        String -- L'inverse complément de la séquence nucléotidique 
    """
    reverse_read = ""
    for base in read:
        reverse_read = REVERSE_DICT[base] + reverse_read
    return reverse_read

@profile
def indexation(nb_reads_ram, k):
    """
    Retourne le dictionnaire d'indexation de tous les reads selon leurs k premiers nucléotides
    
    Keyword Arguments:
        nb_reads_ram {int} -- Le nombre de reads à charger simultanément dans la RAM (default: {100})
        k {int} -- fenêtre k-mer (default: {10})
    
    Returns:
        dict -- indexation
    """
    index = defaultdict(list) 
    for i in range(nb_reads//nb_reads_ram if not nb_reads%nb_reads_ram else nb_reads//nb_reads_ram+1):
        reads = load_reads(i*nb_reads_ram, min((i+1)*nb_reads_ram, nb_reads)) # chargement des reads
        for ix in reads:
            index[reads[ix][:k]].append([ix, 'F']) # Indexation du mode Forward
            index[reverse_seq(reads[ix][-k:])].append([ix, 'R']) # Indexation du mode Reverse
    return dict(index)


def naive_check_ind(s1,s2):
    """ 
    Renvoie l'index i de la première occurence s1 dans s2. Utilisation d'un algorithme de Pattern Matching naïf

    Arguments:
        s1 {String} -- Séquence nucléotidique 1, sous-chaîne espérée de s2
        s2 {String} -- Séquence nucléotidique 2
    Returns:
        int -- index de positionnement de s1 sur s2
    """
    lenght_s1 = len(s1)
    lenght_diff = len(s2)-lenght_s1+1
    for i in range(lenght_diff):
        for j in range(lenght_s1):
            if s1[j] != s2[i+j]:
                break
            if j == lenght_s1-1:
                return i
    return -1

def find_right_overlaps(s1, len_read, bl = list()):
    """
    Renvoie la liste des reads chevauchants par la droite la séquence s1.

    Arguments:
        s1{String} -- Séquence nucléotidique à étendre
        len_read {int} -- Longueur maximale d'un read
        bl {list} -- Liste des index de read à bannir de l'alignement, car déjà positionnés en amont 
    Returns:
        list -- [   Position du début d'alignement du read sur la séquence s1,
                    Séquence nucléotidique du read,
                    Index du read   ]
    """
    reads_list_overlap = list()
    for shift in range(1, len_read-k+1): # Déplacement d'une fenêtre d'alignement query sur la séquence à étendre
        window = s1[shift:shift+k]
        if window not in hash_table.keys(): # Vérification du référencement de cette fenêtre dans la table de hachage
            continue
        else:
            for read in hash_table[window]:
                if read[0] not in bl: # Vérification de la non appartenance du read à la black_list
                    if read[1] == "F": # Chargement du read, en Forward ou Reverse
                        s2 = load_from_directory(reads_path, read[0], format_fasta)
                    else:
                        s2 = reverse_seq(load_from_directory(reads_path, read[0],format_fasta))
                    if len(s2) < len_read - shift:  # Read non chevauchant
                        continue
                    i = shift + k
                    j = k
                    error_acceptation = (len_read - shift - k) * error_rate_admission # Définition d'un seuil d'acceptation d'erreurs d'alignements
                    error_count = 0
                    while i < len_read and error_count <= error_acceptation:   # Boucle d'alignement
                        if s1[i] != s2[j]:
                            error_count += 1
                        i += 1
                        j += 1
                    if i == len_read: # Vérification de l'aboutissement de l'alignement : atteinte de la fin de s1
                        reads_list_overlap.append([len_read - shift, s2, read[0]]) # Ajout du read à la liste des reads chevauchants
    return reads_list_overlap

def assembly(S,read):
    """
    Extension d'une séquence S par un read passé en argument.

    Arguments:
        S{String} -- Séquence nucléotidique à étendre
        read {list} -- [Position du début d'alignement du read sur S, 
                        Séquence nucléotidique du read, 
                        Index du read]
    Returns:
        String -- Renvoie la séquence étendu de S + read
    """
    return S[:-read[0]]+read[1]

def extend(S, black_list = list()):
    """
    Extension de la séquence S vers la droite. Stockage d'un éventuel chemin dans 'all_seq'

    Arguments:
        S{String} -- Séquence nucléotidique à étendre
        black_list {list} -- Liste des index de read à bannir de l'alignement, car déjà positionnés en amont 
    Returns:
        None
    """
    if k_stop in S: # Première condition d'arrêt : k_stop trouvé : mapping réussi
        print("Chemin trouvé")
        all_seq.append(S[:S.index(k_stop)+len(k_stop)]) # La séquence ajoutée est tronquée au dernier nucléotide du k-mer stop
        return
    size_mapping = len(S)
    if size_mapping > len_mapping + 2*max_len_read: # Deuxième condition d'arrêt : Dépassement de la taille attendue de mapping
        return
    overlaps_reads = find_right_overlaps(S[-max_len_read:], min(max_len_read,size_mapping)) # Recherche des reads chevauchant la séquence S
    if not overlaps_reads: # Troisième condition d'arrêt : Aucune potentielle extension trouvée
        return
    sorted_extensions = [overlaps_reads[0]] # Initialisation d'une liste des reads rangés par type d'extension
    group_balance = [1] # Initialisation d'un compteur pour chaque groupe d'extension / Gestion des branches erreurs
    for read in overlaps_reads[1:]: # Attribution des reads à chaque groupe possible, du plus chevauchant au moins chevauchant
        new_extension_type = True
        for count, group in enumerate(sorted_extensions):
            if naive_check_ind(group[1][group[0]:],read[1][read[0]:]) == 0: # Teste si le read appartient au groupe 'group'
                new_extension_type = False
                group_balance[count] += 1
                break
        if new_extension_type: # Condition de création d'un nouveau type d'extension (Nouvelle branche)
            group_balance.append(1)
            sorted_extensions.append(read)
    mean_balance = sum(group_balance)/len(group_balance) # Tri d'erreur basé sur la comparaison au nombre de reads moyens par type d'extension
    for count,read in enumerate(sorted_extensions):
        if group_balance[count] >= mean_balance: # Extension effectuée seulement pour un read d'un type d'extension sureprésenté (1/3)
            extend(assembly(S,read),black_list+[read[2]]) # Récursivité de l'extension

@profile
def main():
    extend(k_start) # Extension de k_start vers k_stop
    # Impression des séquences trouvées
    if all_seq: 
        for seq in all_seq:
            print("Seq :",seq) 
            print("Size :",len(seq))
    else:
        print("Aucun chemin trouvé.")
    
if __name__ == "__main__":
    start = time.time()
    try:
        optlist, args = getopt.getopt(sys.argv[1:], 'k:e:s:') # Sauvegarde des paramètres numériques passés en argument du script
        if len(args) != 2: # Vérifie la présence de deux chemins d'accès dans les arguments
            print("Error: This script expects two filenames as arguments, here:",len(args))
            sys.exit(2)
        optdict = dict()
        for opt in optlist:
            optdict[opt[0]] = opt[1]
        k = int(20 if "-k" not in optdict.keys() else optdict["-k"])  # Attribution d'une valeur k par défaut en cas d'omission
        error_rate_admission = float(0.01 if "-e" not in optdict.keys() else optdict["-e"]) # Attribution du taux d'erreur estimé par défaut si non spécifié
        k_start_stop_path, reads_path = args # Chemin d'accès des k-mers start_stop et des reads
        with open(k_start_stop_path, "r") as k_start_stop_file: # Lecture du fichier contenant les k-mer start et stop
            k_start_stop = k_start_stop_file.read().split('\n')
        with open(reads_path, "r") as reads_file:  # Test du chemin du fichier 2, compteur du nombre de lignes
            if reads_file.readline()[0] == ">": # Vérification format fasta
                format_fasta = True
            else:
                format_fasta = False
            nb_line = 1
            max_len_read = 0
            for line in reads_file:
                if format_fasta and not (nb_line-1)%2: # Enregistrement taille de read max
                    if len(line)-1 > max_len_read :
                        max_len_read = len(line)-1
                elif not format_fasta and not (nb_line-1)%4:
                    if len(line)-1 > max_len_read :
                        max_len_read = len(line)-1
                nb_line += 1
            nb_reads = int(nb_line/2 if format_fasta else nb_line/4) # Nombre de reads attendus pour .fasta ou .fastq
    # Gestion des erreurs
    except getopt.GetoptError as err:
        print(err)  # Affiche l'erreur d'argument lors de l'appel du script
        sys.exit(2)
    except ValueError:
        print("Error: Parameter k expects an integer input")
        sys.exit(2)
    except IOError as e:
        print("Error: {}: {}".format(e.filename, e.strerror))
        sys.exit(2)
    ### Définition des variables globales
    k_start, k_stop = [k_start_stop[1],k_start_stop[3]] # Séquences k-mers start et stop
    if "-s" not in optdict.keys():
        # Estimation automatique de la taille de la séquence à construire
        start_pos = list(map(int, re.findall(r'\d+',k_start_stop[0]))) # Position génomique de start
        stop_pos = list(map(int, re.findall(r'\d+',k_start_stop[2]))) # Position génomique de stop
        len_mapping = stop_pos[1] - start_pos[0] # Taille attendue du mapping (auto)
    else:
        len_mapping = int(optdict["-s"]) # Taille attendue du mapping 
    all_seq = [] # Stocke tous les chemins possibles de k_start vers k_stop 
    print("Nombre de reads reconnus : {}\nLongueur de la séquence à assembler : {} nt\nk_start : {}\nk_stop : {}".format(nb_reads,len_mapping, k_start, k_stop))
    print("Construction table de hachage...")
    hash_table = indexation(10000,k) # Mise au point d'une table de hachage : indexation des reads
    print("Fait.")
    main()
    end = time.time()

    print("Le programme fait  %f seconds de temps d'exécution"%(end - start))    
