# ALG_2019

## Projet Algorithmique des Séquences 2019 :

L'idée est d'implémenter un algorithme d'assemblage OLC permettant de faire du "Gap Filling". Étant donnés un jeu de reads de type Illumina et deux k-mers `start` et `stop`, le but est de reconstruire au moins une séquence, construite à partir des reads chevauchants, débutant par `start` et terminant par `stop`.

- __Deprecated_projet.py__ : Version dépréciée de OLC_assembler.py
- __Folder_test_automation.sh__ : Script de test de l'implémentation d'OLC_assmbler.py sur un dossier contenant des fichiers .fa de reads
    - Arguments :
        - __$1__ : Chemin d'accès du dossier contenant les fichiers de reads à tester (ex : ./data/ecoli/gapped_reads/)
        - __$2__ : Chemin d'accès du fichier contenant les k-mers start et stop (ex : ./data/ecoli/start_stop_2kb.fa)
        - __$3__ : Valeur de la seed k (ex : 10)
        - __$4__ : Valeur du taux d'erreur accepté pour l'alignement (ex : 0.1)
- __Local_alignment_tool.py__ : Script utilisé par Folder_test_automation.sh afin d'estimer le score d'identité d'alignement local avec la séquence de référence (Utilisation de BioPython)
- __OLC_assembler.py__ : Script d'assemblage basé sur le principe OLC
    - Arguments optionnels : 
        - __k__ : taille de la boîte k-mer (*seed* ou *key* du référencement des reads dans la table de hachage)
        - __e__ : fréquence d'erreur attendue lors du séquençage (Permissivité d'erreurs pendant alignements de reads au cours du mapping)
        - __s__ : Taille du mapping attendue (en pb)
    - Arguments obligatoires :
        - Chemin d'accès du fichier fasta contenant les k-mers start et stop
        - Chemin d'accès du fichier fasta ou fastq contenant les reads à mapper
- __Overall_automate.sh__ : Script d'automatisation total : permet le *fine tuning* des paramètres k et e


### Niveau d'implémentation :

- [x] Renvoi d'une séquence mappée
- [x] Gestion des cycles
- [x] Gestion des erreurs
- [x] Rendu du chemin quasi-consensus (non exempt d'erreurs)
- [ ] Portage format .fq
- [ ] Gestion des tailles de reads multiples
- [ ] Gestion de l'utilisation d'une approximation de la taille de mapping (passée en argument du script)
